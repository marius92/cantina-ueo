###Cantina Universitatea Emanuel Oradea###

##Proiect realizat cu Symfony##

* Database name: cantina_ueo
* Database user: root
* -No password-



**Workflow - Git**

1. Exista un branch de master unde se va face push doar cu ce este terminat si functional.
2. In momentul in care se va lucra o noua functionalitate se va face un nou branch.
3. Exista un branch de development unde se va face push dupa ce se termina de facut un nou feature.
4. Ininte de a incepe sa se lucreze se face un pull request - git pull
5. Mesajele de commit sa fie cat mai sugestive.

**Documentatii**
-This site was built using [Twig](https://twig.symfony.com/) , [Symfony](hhttps://symfony.com/) , [Bootstrap](https://getbootstrap.com/).

**Instalare in wampserver - Apache - vhosts**
```
<VirtualHost *:80>
 ServerName cantina-ueo.local
 ServerAlias cantina-ueo.local
 DocumentRoot "***path in pc ***\public"
 <Directory "***path in pc **\public">
	#enable the .htaccess rewrites
   	AllowOverride All
	Require all granted
	#ErrorLog "D:\cantina-ueo\logs\error.log"
   	#CustomLog "D:\cantina-ueo\logs\access.log" combined
 </Directory>
</VirtualHost>
```

***In "hosts" C:\Windows\System32\drivers\etc\hosts***

`127.0.0.1 cantina-ueo.local`

***Comenzi de rulat***
1. cd cantina-ueo
2. yarn install
3. install composer - https://getcomposer.org/download/
4. ***Baza de date***
1. composer require symfony/orm-pack
2. composer require --dev symfony/maker-bundle
3. php bin/console doctrine:migrations:migrate

- composer require symfony/dotenv


***Baza de date***
1. composer require symfony/orm-pack
2. composer require --dev symfony/maker-bundle
3. php bin/console doctrine:migrations:migrate


***Running***
yarn encore dev


