<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AccountantController extends AbstractController
{
    /**
     * @Route("/accountant-dashboard", name="dashboardAccountant")
     */
    public function index()
    {
        return $this->render('accountant/accountant-dashboard.html.twig', [
            'controller_name' => 'AccountantController',
            'numepagina' => 'Dashboard',
        ]);
    }
    /**
     * @Route("/accountant-abonamente", name="abonamente")
     */
    public function abonamente()
    {
        return $this->render('accountant/accountant-abonamente.html.twig', [
            'controller_name' => 'accountantController',
            'numepagina' => "Abonamente",
        ]);
    }
    /**
     * @Route("/accountant-anunturi", name="anunturiAccountant")
     */
    public function anunturi()
    {
        return $this->render('accountant/accountant-anunturi.html.twig', [
            'controller_name' => 'AccountantController',
            'numepagina' => ("Anunțuri"),
        ]);
    }

    /**
     * @Route("/accountant-profile", name="profilAccountant")
     */
    public function profile()
    {
        return $this->render('accountant/accountant-profile.html.twig', [
            'controller_name' => 'AccountantController',
            'numepagina' => "Profil",
        ]);
    }
}
