<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin-dashboard", name="dashboard")
     */
    public function index()
    {
        return $this->render('admin/admin-dashboard.html.twig', [
            'controller_name' => 'AdminController',
            'numepagina' => "Dashboard",
        ]);
    }

    /**
     * @Route("/admin-abonamente", name="abonamente")
     */
    public function abonamente()
    {
        return $this->render('admin/admin-abonamente.html.twig', [
            'controller_name' => 'AdminController',
            'numepagina' => "Abonamente",
        ]);
    }

    /**
     * @Route("/admin-utilizatori", name="utilizatori")
     */
    public function utilizatori()
    {
        return $this->render('admin/admin-utilizatori.html.twig', [
            'controller_name' => 'AdminController',
            'numepagina' => "Utilizatori",
        ]);
    }

    /**
     * @Route("/admin-anunturi", name="anunturi")
     */
    public function anunturi()
    {
        return $this->render('admin/admin-anunturi.html.twig', [
            'controller_name' => 'AdminController',
            'numepagina' => ("Anunțuri"),
        ]);
    }

    /**
     * @Route("/admin-profile", name="profil")
     */
    public function profile()
    {
        return $this->render('admin/admin-profile.html.twig', [
            'controller_name' => 'AdminController',
            'numepagina' => "Profil",
        ]);
    }
}
